'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('main', ['ui.bootstrap']);

//services

//controllers

app.controller('mainCtrl', ['$scope', '$location', '$timeout', '$http', function ($scope, $location, $timeout, $http) {
	
	var timeout;
	
	$scope.filter = function () {
		$timeout.cancel(timeout);
		timeout = $timeout(function() {
			filterFunction();
		},500);
	}
	
	var occuranceCount = function (str,word) {
		return str.split(word).length - 1;
	}
	
	var filterFunction = function () {
		
		$scope.loading = true;
		
		if(!$scope.searchBar || $scope.searchBar.length < 2){
			$scope.pages = originalPages;
		} else {
			
			var words = $scope.searchBar.toUpperCase().split(/\W+/g);
			console.log(words);
			
			var temp = [];
			
			for (var i = 0; i < originalPages.length; i++) {
				var page = originalPages[i];
				
				var count = 0;
				for (var j = 0; j < words.length; j++) {
					var cnt = occuranceCount(page.searchText,words[j]);
					if( cnt > 0){
						count += cnt;
					} else {
						count = 0;
						break;
					}
				}
				
				if(count > 0){
					page.count = count;
					temp.push(page);
				} else {
					page.count = 0;
				}
				
			}
			
			$scope.pages = temp.sort(function (a,b) {
				return a.count - b.count;
			});
			
		}
		
		
		$scope.noResult = $scope.pages.length == 0;
		
		$scope.loading = false;
	}
	
	$scope.open = function (url) {
		window.open(url, '_blank');
	}
	
	$scope.setSearch = function (keyword) {
		$scope.searchBar = keyword;
	}
	
	var originalPages = null;
	
	try {
		
		$scope.loading = true;
		
		$http.get('/pages.json').then(function(response) {
			
			originalPages = response.data;
			for (var i = originalPages.length - 1; i >= 0; i--) {
				var page = originalPages[i];
				page.searchText = [page.title,page.note,page.description,(page.keywords ? page.keywords : '')].join(' ').toUpperCase();
			}

			console.log(originalPages);
			
			filterFunction();
			
		}, function() {
			throw "get fail";
		});
		
		
	} catch (err) {
		console.log(err);
		$scope.pages = [{"title":"Page Error","url":"./"}];
	}
	
}]);

app.controller('suggestCtrl', ['$scope', '$http', function ($scope, $http) {
	function generateEmailBody() {
		$scope.body = encodeURIComponent("Page Name: "+($scope.name?$scope.name:'')+"\nPage Description: "+($scope.descr?$scope.descr:'')+"\nPage URL: "+($scope.url?$scope.url:'')+"\nKeywords: "+($scope.keywords?$scope.keywords:'')+"\n\n"+($scope.comment?$scope.comment:''));
	}
	
	$scope.$watch("name",generateEmailBody);
	$scope.$watch("descr",generateEmailBody);
	$scope.$watch("url",generateEmailBody);
	$scope.$watch("keywords",generateEmailBody);
	$scope.$watch("comment",generateEmailBody);
}]);

//directives


